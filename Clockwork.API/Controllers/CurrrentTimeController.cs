﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace Clockwork.API.Controllers
{
    public class CurrentTimeController : Controller
    {
        // GET api/currenttime
        [HttpGet]
        [Route("api/[controller]")]
        public IActionResult GetTime()
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();
            var timeZone = "EST";

            var returnVal = new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime,
                TimeZone = timeZone
            };

            using (var db = new ClockworkContext())
            {
                db.CurrentTimeQueries.Add(returnVal);
                var count = db.SaveChanges();
                Console.WriteLine("{0} records saved to database", count);

                Console.WriteLine();
                foreach (var CurrentTimeQuery in db.CurrentTimeQueries)
                {
                    Console.WriteLine(" - {0}", CurrentTimeQuery.UTCTime);
                }
            }

            return Ok(returnVal);
        }

        // GET api/currenttime/timezone
        [HttpGet]
        [Route("api/[controller]/{tz}")]
        public IActionResult GetTime(string tz)
        {
            var tzInfo = TimeZoneInfo.FindSystemTimeZoneById(tz);
            var utcTime = DateTime.UtcNow;

            var serverTime = TimeZoneInfo.ConvertTime(DateTime.Now, tzInfo);
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();
            var timeZone = tz;

            var returnVal = new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime,
                TimeZone = timeZone
            };

            using (var db = new ClockworkContext())
            {
                db.CurrentTimeQueries.Add(returnVal);
                var count = db.SaveChanges();
                Console.WriteLine("{0} records saved to database", count);

                Console.WriteLine();
                foreach (var CurrentTimeQuery in db.CurrentTimeQueries)
                {
                    Console.WriteLine(" - {0}", CurrentTimeQuery.UTCTime);
                }
            }

            return Ok(returnVal);
        }

        [HttpGet]
        [Route("api/[controller]/[action]")]
        public IActionResult GetAll()
        {
            var allTimeQuery = new List<CurrentTimeQuery>();
            using (var db = new ClockworkContext())
            {
                allTimeQuery = db.CurrentTimeQueries.ToList();
            }
            return Ok(allTimeQuery);
        }

        [HttpGet]
        [Route("api/[controller]/[action]")]
        public IActionResult TimeZone()
        {
            var allTimeQuery = new List<CurrentTimeQuery>();
            using (var db = new ClockworkContext())
            {
                allTimeQuery = db.CurrentTimeQueries.ToList();
            }
            return Ok(allTimeQuery);
        }
    }
}
