﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Clockwork.API.Models
{
    public class TimeZones
    {
        public string EasternTimeZone { get; set; }
        public string MidWayIsland { get; set; }
        public string Alaska { get; set; }
        public string PacificTime { get; set; }
        public string MountainTime { get; set; }
        public string CentralTime { get; set; }
        public string AtlanticTime { get; set; }
        public string BritishDaylightTime { get; set; }
        public string IndiaStandardTime { get; set; }

    }
}
