﻿document.addEventListener("DOMContentLoaded", function (event) {
    document.getElementById("timeZonePicker").selectedIndex = "21";
    GetAll();
});
let allTimeRequests = [];
function UserAction() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        let singleTimeCard = document.getElementById("singleTimeCard");
        if (this.readyState == 4 && this.status == 200) {
            var cardRequest = JSON.parse(this.responseText);
            console.log(cardRequest);
            //cleanCurrentCard();
            renderCard(cardRequest);
        }
    };
    var uri = "http://localhost:5000/api/CurrentTime/" + document.getElementById("timeZonePicker").value;
    xhttp.open("GET", uri, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
    ChangeVisibility();
    GetAll();
}
function GetAll() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            allTimeRequests = JSON.parse(this.responseText);
            renderCardDeck(allTimeRequests);
            console.log(allTimeRequests);
        }
    };
    var uri = "http://localhost:5000/api/CurrentTime/GetAll";
    xhttp.open("GET", uri, true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}
function renderCardDeck() {
    let cardDeck = document.getElementById("card-container");
    jQuery.each(allTimeRequests, function (i, val) {
        cardDeck.append(CreateACard(val));
    })
};
function renderCard(val) {
    let cardHolder = document.getElementById("singleTimeCard");
    cardHolder.append(CreateACard(val))
}
function cleanCurrentCard(){
    document.getElementById("singleTimeCard").innerHTML = "";
}
function ChangeVisibility() {
    document.getElementById("singleTimeCard").className = "row m-2";
}
function CreateACard(timeStamp) {
    let card = new DocumentFragment();
    let carddiv = document.createElement('div');
    carddiv.classList.add("card", "mx-auto", "text-center");
    carddiv.style = "width: 18rem; margin-bottom: 5px";

    let cardtitle = document.createElement('div');
    cardtitle.className = "card-header", "text-white";
    cardtitle.style = "background-color: #00AA9E;", "text-white";
    cardtitle.innerText = 'QueryId:' + timeStamp.currentTimeQueryId;
    carddiv.appendChild(cardtitle);

    let cardbody = document.createElement('div');
    cardbody.className = 'card-body';


    let cardtext = document.createElement('h5');
    cardtext.className = "text";
    cardtext.id = timeStamp.currentTimeQueryId;
    let dateTime = new Date(timeStamp.time);
    cardtext.innerText =  timeStamp.timeZone + ": " + dateTime.toString().split(" GMT")[0];
    cardbody.appendChild(cardtext);

    let cardsmalltext = document.createElement('h5');
    cardsmalltext.id = timeStamp.currentTimeQueryId + "UTC"
    cardsmalltext.className = "text";
    let dateTimeUTC = new Date();
    var d = dateTimeUTC.toUTCString();
    cardsmalltext.innerText = "UTC Time: " + d.toString().split(" GMT")[0];
    cardbody.appendChild(cardsmalltext);
    carddiv.appendChild(cardbody);
    card.append(carddiv);
    return card;
}



//document.querySelector(".js-Selector").addEventListener("change", e => {
//    jQuery.each(allTimeRequests, function (i, val) {
//        let dateTimeUtc = new Date(val.utcTime);
//        let dateTimeLocal = new Date(dateTimeUtc.getTime() + e.target.value * 3600 * 1000);
//        document.getElementById(i + 1).innerHTML = "Local Requested Time: " + dateTimeLocal.toString().split(" GMT")[0];
//    });
//});

//const event = new Event("change");
//document.querySelector(".js-Selector").dispatchEvent(event);
